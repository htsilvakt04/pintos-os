-----------------
Pintos is a teaching operating system for 32-bit x86, challenging but not overwhelming, small but realistic enough to understand OS in depth (it can run on x86 machine and simulators including QEMU, Bochs and VMWare Player!). The main source code, documentation and assignments are developed by Ben Pfaff and others from Stanford (refer to its LICENSE).
-----------------

**Project 1: Loading User program.**
- Add the ability to read command-line arguments and make system calls.
- Passed all tests.
<img src="result/user-prog.png" alt="Project 1 Result" height="300">

**Project 2: Adding Threads and Synchonization primitives.**
- Adding threads and Priority Scheduler.
- Passed all tests (12 left is stmf which is not required).
<img src="result/threads.png" alt="Project 2 Result" height="300">


**Project 3: Adding Fast File System.**
- 141/142 tests passed.
<img src="result/filesys.png" alt="Project 3 Result" height="300">
