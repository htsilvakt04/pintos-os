/* Try reading a file in the most normal way. */

#include "tests/userprog/sample.inc"
#include "tests/lib.h"
#include "tests/main.h"

// void check_file(const char* file_name, const void* buf, size_t filesize);
void test_main(void) { check_file("sample.txt", sample, sizeof sample - 1); }
