/* Wait for a subprocess to finish. */

#include <syscall.h>
#include "tests/lib.h"
#include "tests/main.h"

void test_main(void) {
	int pid = exec("child-simple");
	int status = wait(pid);
	msg("wait(exec()) = %d", status);
}
