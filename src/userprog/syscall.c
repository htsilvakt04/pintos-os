#include "userprog/syscall.h"
#include "userprog/process.h"
#include "userprog/pagedir.h"
#include "userprog/syscall-util.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "threads/synch.h"
#include "lib/kernel/console.h"
#include "lib/float.h"
#include "filesys/file.h"
#include "filesys/filesys.h"

static void syscall_handler(struct intr_frame *);
static void write_syscall(struct intr_frame *, uint32_t *, struct thread *);
static void read_syscall(struct intr_frame *, uint32_t *, struct thread *);
static void filesize_syscall(struct intr_frame *, uint32_t *, struct thread *);
static void seek_syscall(struct intr_frame *, uint32_t *, struct thread *);
static void tell_syscall(struct intr_frame *, uint32_t *, struct thread *);
static void exit_syscall(struct intr_frame *, uint32_t *);
static void open_syscall(struct intr_frame *, uint32_t *, struct thread *);
static void close_syscall(struct intr_frame *, uint32_t *, struct thread *);
static void create_syscall(struct intr_frame *, uint32_t *);
static void remove_syscall(struct intr_frame *, uint32_t *);
static void exec_syscall(struct intr_frame *, uint32_t *);
static void wait_syscall(struct intr_frame *, uint32_t *);
static void practice_syscall(struct intr_frame *, uint32_t *);
static void compute_e_syscall(struct intr_frame *, uint32_t *);
static void sys_pthread_create(struct intr_frame *, uint32_t *);
static void sys_pthread_exit(struct intr_frame *, uint32_t *);
static void sys_pthread_join(struct intr_frame *f, uint32_t *args);
static void sys_sema_init(struct intr_frame *f, uint32_t *args);
static void sys_sema_down(struct intr_frame *f, uint32_t *args);
static void sys_sema_up(struct intr_frame *f, uint32_t *args);
static void sys_lock_init(struct intr_frame *f, uint32_t *args);
static void sys_lock_acquire(struct intr_frame *f, uint32_t *args);
static void sys_lock_release(struct intr_frame *f, uint32_t *args);
static void chdir_syscall(struct intr_frame *, uint32_t *, struct thread *);
static void mkdir_syscall(struct intr_frame *, uint32_t *);
static void readdir_syscall(struct intr_frame *, uint32_t *, struct thread *);
static void isdir_syscall(struct intr_frame *, uint32_t *, struct thread *);
static void inumber_syscall(struct intr_frame *, uint32_t *, struct thread *);
static void cache_invalidate_syscall(void);
static void cache_hit_syscall(struct intr_frame *);
static void cache_miss_syscall(struct intr_frame *);
static void cache_write_count_syscall(struct intr_frame *);
static void cache_read_count_syscall(struct intr_frame *);

void syscall_init(void) { intr_register_int(0x30, 3, INTR_ON, syscall_handler, "syscall"); }

static void syscall_handler(struct intr_frame *f) {
	uint32_t *args = ((uint32_t *)f->esp);
	struct thread *t = thread_current();
	validate_pointer(f, args, sizeof(uint32_t));
	/*
	 * The following print statement, if uncommented, will print out the syscall
	 * number whenever a process enters a system call. You might find it useful
	 * when debugging. It will cause tests to fail, however, so you should not
	 * include it in your final submission.
	 */

//	printf("System call number: %d\n", args[0]);

	switch (args[0]) {
		case SYS_COMPUTE_E: compute_e_syscall(f, args);
			break;
		case SYS_SEEK: seek_syscall(f, args, t);
			break;
		case SYS_TELL: tell_syscall(f, args, t);
			break;
		case SYS_CLOSE: close_syscall(f, args, t);
			break;
		case SYS_REMOVE: remove_syscall(f, args);
			break;
		case SYS_CREATE: create_syscall(f, args);
			break;
		case SYS_FILESIZE: filesize_syscall(f, args, t);
			break;
		case SYS_OPEN: open_syscall(f, args, t);
			break;
		case SYS_WRITE: write_syscall(f, args, t);
			break;
		case SYS_READ: read_syscall(f, args, t);
			break;
		case SYS_EXIT: exit_syscall(f, args);
			break;
		case SYS_EXEC: exec_syscall(f, args);
			break;
		case SYS_WAIT: wait_syscall(f, args);
			break;
		case SYS_PRACTICE: practice_syscall(f, args);
			break;
		case SYS_PT_CREATE: sys_pthread_create(f, args);
			break;
		case SYS_PT_EXIT: sys_pthread_exit(f, args);
			break;
		case SYS_PT_JOIN: sys_pthread_join(f, args);
			break;
		case SYS_GET_TID: f->eax = t->tid;
			break;
		case SYS_SEMA_INIT: sys_sema_init(f, args);
			break;
		case SYS_SEMA_DOWN: sys_sema_down(f, args);
			break;
		case SYS_SEMA_UP: sys_sema_up(f, args);
			break;
		case SYS_LOCK_INIT: sys_lock_init(f, args);
			break;
		case SYS_LOCK_ACQUIRE: sys_lock_acquire(f, args);
			break;
		case SYS_LOCK_RELEASE: sys_lock_release(f, args);
			break;
	}
}

static void sys_lock_acquire(struct intr_frame *f, uint32_t *args) {
	// set the default return
	f->eax = false;
	struct thread *t = thread_current();
	validate_pointer(f, args[1], sizeof(sema_t * ));
	lock_t *lock = args[1];

	// check whether the lock has registered with the kernel
	struct list_elem *e;
	struct lock_info *l = NULL;

	/* Check whether the sema has registered with the kernel before */
	for (e = list_begin(&t->pcb->locks); e != list_end(&t->pcb->locks);
			 e = list_next(e)) {
		l = list_entry(e,
		struct lock_info, elem);
		if (l->lt == lock)
			break;
		l = NULL;
	}
	// check if the thread already hold the thread
	if (l == NULL || l->lock.holder == t) return;
	// acquire the lock
	lock_acquire(&l->lock);
	// return
	f->eax = true;
}

static void sys_lock_release(struct intr_frame *f, uint32_t *args) {
	// set the default return
	f->eax = false;
	struct thread *t = thread_current();
	validate_pointer(f, args[1], sizeof(sema_t * ));
	lock_t *lock = args[1];

	// check whether the lock has registered with the kernel
	struct list_elem *e;
	struct lock_info *l = NULL;

	/* Check whether the sema has registered with the kernel before */
	for (e = list_begin(&t->pcb->locks); e != list_end(&t->pcb->locks);
			 e = list_next(e)) {
		l = list_entry(e,
		struct lock_info, elem);
		if (l->lt == lock)
			break;
		l = NULL;
	}

	// check if the thread already hold the thread
	if (l == NULL || l->lock.holder != t) return;
	// acquire the lock
	lock_release(&l->lock);
	// return
	f->eax = true;
}

static void sys_lock_init(struct intr_frame *f, uint32_t *args) {
	f->eax = false;
	struct thread *t = thread_current();
	lock_t *lock = args[1];
	if (lock == NULL || !is_user_vaddr(lock + sizeof(lock_t * )))
		return;

	struct lock_info *l = malloc(sizeof(struct lock_info));
	if (l == NULL) return;
	// set the fields in the struct lock_info
	l->lt = lock;
	lock_init(&l->lock);
	// add to the process
	list_push_back(&t->pcb->locks, &l->elem);

	f->eax = true;
}

static void sys_sema_init(struct intr_frame *f, uint32_t *args) {
	f->eax = false;
	struct thread *t = thread_current();
	sema_t *sema = args[1];
	int val = args[2];
	if (val < 0 || sema == NULL || !is_user_vaddr(sema + sizeof(sema_t * )))
		return;

	struct sema_info *l = malloc(sizeof(struct sema_info));
	if (l == NULL)
		return;
	l->st = sema;
	sema_init(&l->sema, val);
	// add to the process
	list_push_back(&t->pcb->semaphores, &l->elem);
	// return
	f->eax = true;
}

static void sys_sema_down(struct intr_frame *f, uint32_t *args) {
	struct thread *t = thread_current();
	validate_pointer(f, args[1], sizeof(sema_t * ));
	sema_t *sema = args[1];
	// set it to false as default
	f->eax = false;
	struct list_elem *e;
	struct sema_info *p = NULL;

	/* Check whether the sema has registered with the kernel before */
	for (e = list_begin(&t->pcb->semaphores); e != list_end(&t->pcb->semaphores);
			 e = list_next(e)) {
		p = list_entry(e,
		struct sema_info, elem);
		if (p->st == sema)
			break;
		p = NULL;
	}

	if (p == NULL) return;
	// down the sema
	sema_down(&p->sema);
	f->eax = true;
}

static void sys_sema_up(struct intr_frame *f, uint32_t *args) {
	struct thread *t = thread_current();
	validate_pointer(f, args[1], sizeof(sema_t * ));
	sema_t *sema = args[1];
	// set it to false as default
	f->eax = false;
	struct list_elem *e;
	struct sema_info *p = NULL;

	/* Check whether the sema has registered with the kernel before */
	for (e = list_begin(&t->pcb->semaphores); e != list_end(&t->pcb->semaphores);
			 e = list_next(e)) {
		p = list_entry(e,
		struct sema_info, elem);
		if (p->st == sema)
			break;
		p = NULL;
	}

	if (p != NULL) {
		// up the sema
		sema_up(&p->sema);
		f->eax = true;
	}
}

static void sys_pthread_join(struct intr_frame *f, uint32_t *args) {
	f->eax = pthread_join(args[1]);
}

static void sys_pthread_exit(struct intr_frame *f, uint32_t *args) {
	struct thread *t = thread_current();
	if (t == t->pcb->main_thread) {
		args[1] = 0;
		exit_syscall(f, args);
	} else
		pthread_exit(f->esp);
}

static void sys_pthread_create(struct intr_frame *f, uint32_t *args) {
	f->eax = pthread_execute((stub_fun)args[1], (pthread_fun)args[2], (void *)args[3]);
}
static void exec_syscall(struct intr_frame *f, uint32_t *args) {
	validate_pointer(f, args, 8);
	validate_pointer(f, args[1], 4);

	char *cmd_line = args[1];
	int pid = process_execute(cmd_line);
	f->eax = pid;
}
static void compute_e_syscall(struct intr_frame *f, uint32_t *args) {
	validate_pointer(f, args, 2 * sizeof(uint32_t));
	int n = args[1];

	f->eax = sys_sum_to_e(n);
}

// void seek (int fd, unsigned position)
static void seek_syscall(struct intr_frame *f, uint32_t *args, struct thread *t) {
	validate_pointer(f, args, 3 * sizeof(uint32_t));
	unsigned fd = args[1];
	unsigned position = args[2];
	if (fd == STDOUT_FILENO || fd == STDIN_FILENO || !check_fd(t->pcb, fd)) {
		f->eax = -1;
		return;
	}
	lock_acquire(&file_sys_lock);
	file_seek(t->pcb->file_table[fd], position);
	lock_release(&file_sys_lock);
}

// unsigned tell(int fd)
static void tell_syscall(struct intr_frame *f, uint32_t *args, struct thread *t) {
	validate_pointer(f, args, 2 * sizeof(uint32_t));
	unsigned fd = args[1];

	if (fd == STDOUT_FILENO || fd == STDIN_FILENO || !check_fd(t->pcb, fd)) {
		f->eax = -1;
		return;
	}
	lock_acquire(&file_sys_lock);
	f->eax = file_tell(t->pcb->file_table[fd]);
	lock_release(&file_sys_lock);
}

// void close (int fd)
static void close_syscall(struct intr_frame *f, uint32_t *args, struct thread *t) {
	validate_pointer(f, args, 2 * sizeof(uint32_t));
	unsigned fd = args[1];

	if (fd == STDOUT_FILENO || fd == STDIN_FILENO || !check_fd(t->pcb, fd)) {
		f->eax = -1;
		return;
	}
	lock_acquire(&file_sys_lock);
	file_close(t->pcb->file_table[fd]);
	lock_release(&file_sys_lock);
	t->pcb->file_table[fd] = NULL;
	f->eax = 0;
}

static void remove_syscall(struct intr_frame *f, uint32_t *args) {
	validate_pointer(f, args, 2 * sizeof(uint32_t));
	validate_pointer(f, args[1], 0);
	char *file = args[1];
	lock_acquire(&file_sys_lock);
	f->eax = filesys_remove(file);
	lock_release(&file_sys_lock);
}

// bool create (const char *file, unsigned initial_size)
static void create_syscall(struct intr_frame *f, uint32_t *args) {
	validate_pointer(f, args, 3 * sizeof(uint32_t));
	validate_pointer(f, args[1], 0);
	char *file = args[1];
	unsigned size = args[2];

	lock_acquire(&file_sys_lock);
	bool success = filesys_create(file, size);
	lock_release(&file_sys_lock);

	f->eax = success;
}

static void filesize_syscall(struct intr_frame *f, uint32_t *args, struct thread *t) {
	validate_pointer(f, args, 2 * sizeof(uint32_t));
	unsigned fd = args[1];

	if (fd == STDOUT_FILENO || fd == STDIN_FILENO || !check_fd(t->pcb, fd)) {
		f->eax = -1;
		return;
	}
	lock_acquire(&file_sys_lock);
	f->eax = file_length(t->pcb->file_table[fd]);
	lock_release(&file_sys_lock);
}
static void open_syscall(struct intr_frame *f, uint32_t *args, struct thread *t) {
	validate_pointer(f, args, 2 * sizeof(uint32_t));
	validate_pointer(f, args[1], 0);
	char *file_path = args[1];

	lock_acquire(&file_sys_lock);
	struct file *file = filesys_open(file_path);
	lock_release(&file_sys_lock);

	if (file == NULL) {
		f->eax = -1;
		return;
	}

	int fd = assign_file_to_fd(t, file);

	f->eax = fd;
}
static void wait_syscall(struct intr_frame *f, uint32_t *args) {
	validate_pointer(f, args, 2 * sizeof(uint32_t));
	unsigned pid = args[1];
	int exit_code = process_wait(pid);

	f->eax = exit_code;
}
static void read_syscall(struct intr_frame *f, uint32_t *args, struct thread *t) {
	validate_pointer(f, args, 4 * sizeof(uint32_t));
	unsigned fd = args[1];
	unsigned size = args[3];
	validate_pointer(f, args[2], size + 1);
	char *buffer = args[2];

	if (fd == STDOUT_FILENO || !check_fd(t->pcb, fd)) {
		f->eax = -1;
		return;
	}

	if (fd == STDIN_FILENO) {
		int count = 0;
		char c;
		for (int i = 0; i < size && buffer[i] != NULL; ++i) {
			c = input_getc();
			count++;

			if (c == '\n' || c == '\r') {
				buffer[i] = '\0';
				break;
			} else
				buffer[i] = c;
		}

		f->eax = count;
		return;
	}
	lock_acquire(&file_sys_lock);
	int c = file_read(t->pcb->file_table[fd], buffer, size);
	lock_release(&file_sys_lock);
	f->eax = c;
}
static void write_syscall(struct intr_frame *f, uint32_t *args, struct thread *t) {
	validate_pointer(f, args, 4 * sizeof(uint32_t));
	validate_pointer(f, (void *)args[2], (unsigned)args[3]);

	unsigned fd = args[1];
	const void *buffer = (void *)args[2];
	unsigned size = args[3];

	if (fd == 1 || fd == 2) {
		putbuf(buffer, size);
		f->eax = size;
	} else {
		// check if this process owns the "fd"
		if (!check_fd(t->pcb, fd))
			EXIT_WITH_ERROR(f);

		struct file *file = t->pcb->file_table[fd];

		lock_acquire(&file_sys_lock);
		unsigned k = file_write(file, buffer, size);
		lock_release(&file_sys_lock);
		f->eax = k;
	}
}

static void practice_syscall(struct intr_frame *f, uint32_t *args) {
	f->eax = args[1] + 1;
}

static void exit_syscall(struct intr_frame *f, uint32_t *args) {
	validate_pointer(f, args, 2 * sizeof(uint32_t));
	struct thread *t = thread_current();
	f->eax = args[1];
	t->pcb->cps->exit_code = args[1];

	process_exit();
}