#include "syscall-util.h"
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "userprog/syscall.h"
#include "userprog/process.h"
#include "filesys/file.h"
#include "filesys/filesys.h"
#include "threads/vaddr.h"

int assign_file_to_fd(struct thread *t, struct file *file) {
	int i;
	for (i = 3; i < MAX_OPENED_FILES; ++i) {
		if (t->pcb->file_table[i] == NULL) {
			t->pcb->file_table[i] = file;
			return i;
		}
	}

	return -1;
}
bool check_fd(struct process *pcb, int fd) {
	if (fd > MAX_OPENED_FILES || fd < 0) {
		return false;
	}

	if (pcb->file_table[fd] == NULL) {
		return false;
	}

	return true;
}

bool valid_pointer(void *pointer, size_t len) {
	/* Make sure that the pointer doesn't leak into kernel memory */
	return pointer != NULL && is_user_vaddr(pointer + len)
		&& pagedir_get_page(thread_current()->pcb->pagedir, pointer + len) != NULL;
}

void validate_pointer(struct intr_frame *f, void *pointer, size_t len) {
	if (!valid_pointer(pointer, len)) {
		EXIT_WITH_ERROR(f);
	}
}

void EXIT_WITH_ERROR(struct intr_frame *f) {
	struct thread *t = thread_current();

	f->eax = -1;
	t->pcb->cps->exit_code = -1;
//	printf("%s: exit(%d)\n", t->name, -1);
	process_exit();
	NOT_REACHED();
};


