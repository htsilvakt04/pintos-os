#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H
#include "threads/interrupt.h"


void syscall_init(void);
void EXIT_WITH_ERROR(struct intr_frame *);

#endif /* userprog/syscall.h */
