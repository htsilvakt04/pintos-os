//
// Created by silva on 04/02/2023.
//

#ifndef SRC_USERPROG_SYSCALL_UTIL_H_
#define SRC_USERPROG_SYSCALL_UTIL_H_
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "userprog/syscall.h"
#include "userprog/process.h"
#include "filesys/file.h"
#include "filesys/filesys.h"

int assign_file_to_fd(struct thread *t, struct file *file);
bool check_fd(struct process *pcb, int fd);

bool valid_pointer(void *pointer, size_t len);

void validate_pointer(struct intr_frame *f, void *pointer, size_t len);
void EXIT_WITH_ERROR(struct intr_frame *f);
#endif //SRC_USERPROG_SYSCALL_UTIL_H_
