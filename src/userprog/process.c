#include "userprog/process.h"
#include <debug.h>
#include <inttypes.h>
#include <round.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "userprog/gdt.h"
#include "userprog/pagedir.h"
#include "userprog/tss.h"
#include "filesys/directory.h"
#include "filesys/file.h"
#include "filesys/filesys.h"
#include "filesys/directory.h"
#include "threads/flags.h"
#include "threads/init.h"
#include "threads/interrupt.h"
#include "threads/malloc.h"
#include "threads/palloc.h"
#include "threads/synch.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
/* Global log for fileSys */
struct lock file_sys_lock;
static thread_func start_process
NO_RETURN;
static thread_func start_pthread
NO_RETURN;
static bool load(const char *file_name, void (**eip)(void), void **esp);
void fill_stack(char *argv[], size_t argc, struct intr_frame *if_);
bool setup_thread(void (**eip)(void) UNUSED, void **esp UNUSED,
									struct start_pthread_args * args);
static void free_acquired_locks(struct process *);

/* Initializes user programs in the system by ensuring the main
   thread has a minimal PCB so that it can execute and wait for
   the first user process. Any additions to the PCB should be also
   initialized here if main needs those members */
void userprog_init(void) {
	struct thread *t = thread_current();
	bool success;
	
	// make a global lock for file system
	lock_init(&file_sys_lock);

	/* Allocate process control block
	   It is imoprtant that this is a call to calloc and not malloc,
	   so that t->pcb->pagedir is guaranteed to be NULL (the kernel's
	   page directory) when t->pcb is assigned, because a timer interrupt
	   can come at any time and activate our pagedir */
	t->pcb = calloc(sizeof(struct process), 1);
	success = t->pcb != NULL;
	list_init(&t->pcb->child_process_list);
	/* Kill the kernel if we did not succeed */
	ASSERT(success);
}

/* Starts a new thread running a user program loaded from
   FILENAME.  The new thread may be scheduled (and may even exit)
   before process_execute() returns.  Returns the new process's
   process id, or TID_ERROR if the thread cannot be created. */
pid_t process_execute(const char *file_name) {
	char *fn_copy;
	tid_t tid;

	// create a data structure to keep track of the relationship between
		// the parent thread(the thread that create this new thread through process_execute())
		// and the created thread
	struct process_args process_args;
	struct child_parent_status *cps = malloc (sizeof (struct child_parent_status));
	struct thread *t = thread_current ();

	// init
	process_args.success = true;
	cps->ref_count = 2;
	sema_init (&cps->wait_sema, 0);
	lock_init (&cps->ref_count_lock);
	process_args.cps = cps;

	/* Make a copy of FILE_NAME.
	   Otherwise there's a race between the caller and load(). */
	fn_copy = palloc_get_page(0);
	if (fn_copy == NULL)
		return TID_ERROR;
	strlcpy(fn_copy, file_name, PGSIZE);

	/* Make space for storing file_name that will be used to
     name the thread. */
	size_t len = (strcspn(file_name, " ") + 1) * sizeof(char);
	char *thread_name = malloc(len);
	/* Make a copy of the file_name only. */
	strlcpy(thread_name, file_name, len);
	process_args.fn = fn_copy;

	/* Create a new thread to execute FILE_NAME. */
	tid = thread_create(thread_name, PRI_DEFAULT, start_process, &process_args);

	if (tid == TID_ERROR) {
		palloc_free_page(fn_copy);
		return TID_ERROR;
	}
	// wait for the child thread to be created
	sema_down(&cps->wait_sema);

	if(!process_args.success)
		return TID_ERROR;

	// If success, then push the cps into the process list
	list_push_back (&(t->pcb->child_process_list), &(cps->elem));

	return tid;
}

/* A thread function that loads a user process and starts it
   running. */
static void start_process(void * args) {
	struct process_args * process_args = (struct process_args *) args;
	char *file_name = (char *) process_args->fn;
	struct child_parent_status *cps = process_args->cps;

	struct thread *t = thread_current();
	struct intr_frame if_;
	cps->pid = t->tid;
	bool success, pcb_success;

	/* Allocate process control block */
	struct process *new_pcb = malloc(sizeof(struct process));
	success = pcb_success = new_pcb != NULL;

	char *argv[128];
	size_t argc = 0;
	char *save_ptr;

	char *token = strtok_r(file_name, " ", &save_ptr);

	while (token != NULL) {
		argv[argc++] = token;
		token = strtok_r(NULL, " ", &save_ptr);
	}

	argv[argc] = NULL;


	/* Initialize process control block */
	if (success) {
		// Ensure that timer_interrupt() -> schedule() -> process_activate()
		// does not try to activate our uninitialized pagedir
		new_pcb->pagedir = NULL;
		t->pcb = new_pcb;
		t->pcb->cps = malloc(sizeof (struct child_parent_status));
		// Associate the cps with the current process (which is the child process)
		t->pcb->cps = cps;
		t->pcb->num_pthreads = 0;
		for (int i = 0; i < MAX_OPENED_FILES; ++i) {
			t->pcb->file_table[i] = NULL;
		}

		list_init(&t->pcb->child_process_list);
		list_init(&t->pcb->pthreads);
		list_init(&t->pcb->locks);
		list_init(&t->pcb->semaphores);
		sema_init(&t->pcb->main_thread_join_sema, 0);
		// Continue initializing the PCB as normal
		t->pcb->main_thread = t;
		strlcpy(t->pcb->process_name, t->name, sizeof t->name);
	}

	/* Initialize interrupt frame and load executable. */
	if (success) {
		memset(&if_, 0, sizeof if_);
		if_.gs = if_.fs = if_.es = if_.ds = if_.ss = SEL_UDSEG;
		if_.cs = SEL_UCSEG;
		if_.eflags = FLAG_IF | FLAG_MBS;
		success = load(file_name, &if_.eip, &if_.esp);
	}

	if (success)
		fill_stack(argv, argc, &if_);

	// communicate back the success status of lunching a new process
	process_args->success = success;

	/* Handle failure with succesful PCB malloc. Must free the PCB */
	if (!success && pcb_success) {
		// Avoid race where PCB is freed before t->pcb is set to NULL
		// If this happens, then an unfortuantely timed timer interrupt
		// can try to activate the pagedir, but it is now freed memory
		struct process *pcb_to_free = t->pcb;
		t->pcb = NULL;
		free(pcb_to_free);
	}

	/* Clean up. Exit on failure or jump to userspace */
	palloc_free_page(file_name);

	if (!success) {
		cps->exit_code = -1;
		sema_up (&cps->wait_sema);
		if_.eax = -1;
		thread_exit();
	}

	/* Added FPU code */
	int FPU_SIZE = 108;
	uint8_t fpu1[FPU_SIZE];
	uint8_t init_fpu1[FPU_SIZE];
	asm("fsave (%0); fninit; fsave (%1); frstor (%0)" : : "g"(&fpu1), "g"(&init_fpu1));
	for (int i = 0; i < FPU_SIZE; i++) {
		if_.st[i] = init_fpu1[i];
	}

	// wake up the parent
	sema_up (&cps->wait_sema);

	/* Start the user process by simulating a return from an
	   interrupt, implemented by intr_exit (in
	   threads/intr-stubs.S).  Because intr_exit takes all of its
	   arguments on the stack in the form of a `struct intr_frame',
	   we just point the stack pointer (%esp) to our stack frame
	   and jump to it. */
	asm volatile("movl %0, %%esp; jmp intr_exit" : : "g"(&if_) : "memory");
	NOT_REACHED();
}

void fill_stack(char *argv[], size_t argc, struct intr_frame *if_) {
	/* Fake return address is 0 */
	size_t fake_ra = 0;

	/* Push words to top of stack frame */
	for (size_t index = 0; index < argc; index++) {
		/* Get the length of current word */
		size_t arg_size = strlen((char *) argv[index]) + 1;
		/* Decrement user stack pointer accordingly */
		if_->esp -= arg_size;
		/* Copy word to user stack from argv */
		memcpy(if_->esp, argv[index], arg_size);

		/* Add the current stack pointer (address of current word) to argument adresses */
		argv[index] = if_->esp;
	}

	// to be honest: this is an art
	const int align_size = ((size_t)(if_->esp) - ((argc + 3) * 4)) % 16;
	if_->esp -= align_size;
	memset(if_->esp, 0x11, align_size); // 0x11 for debug purpose

	/* Push NULL pointer sentinel (according to 3.1.9 in spec) */
	/* argv[argc] == NULL */
	if_->esp -= 4;
	memset(if_->esp, 0, 4);

	/* ----- (argc + 3) * 4 === argc * 4 + 3 * 4  -----*/
	//	Push argv char pointers (right to left order)
	if_->esp -= argc * 4;
	memcpy(if_->esp, argv, argc * 4);

	//  Push argv
	if_->esp -= 4;
	*((char ***) (if_->esp)) = if_->esp + 4; // the 1st elem of the stack

	//  Push argc
	if_->esp -= 4;
	*((int *) (if_->esp)) = argc;

	//  Push fake return address
	if_->esp -= 4;
	memcpy(if_->esp, &fake_ra, 4);

//	 hex_dump((int) if_->esp, if_->esp, (int) PHYS_BASE - (int) (if_->esp), true);
//	printf("------ esp before leaving fill_Stack: %x\n", if_->esp);
}


/* Waits for process with PID child_pid to die and returns its exit status.
   If it was terminated by the kernel (i.e. killed due to an
   exception), returns -1.  If child_pid is invalid or if it was not a
   child of the calling process, or if process_wait() has already
   been successfully called for the given PID, returns -1
   immediately, without waiting.

   This function will be implemented in problem 2-2.  For now, it
   does nothing. */
int process_wait(pid_t child_pid UNUSED) {
	if (child_pid < 0)
		return TID_ERROR;

	struct thread *t = thread_current ();
	bool is_child = false;
	struct child_parent_status * child = NULL;
	struct list_elem *e;

	for (e = list_begin (&(t->pcb->child_process_list)); e != list_end (&(t->pcb->child_process_list)); e = list_next (e))
	{
		child = list_entry (e, struct child_parent_status, elem);

		if (child->pid == (pid_t) child_pid) {
			is_child = true;
			break;
		}
	}

	if (!is_child || child->exit_code == -1)
		return -1;

	sema_down(&(child->wait_sema));
	list_remove (&child->elem);

	return child->exit_code;
}
static void
free_finished_processes (struct list * cps_list)
{
	struct child_parent_status * finished_progs[list_size (cps_list)];
	int size = 0;
	struct list_elem *e;
	struct child_parent_status *child;

	for (e = list_begin (cps_list); e != list_end (cps_list); e = list_next (e))
	{
		child = list_entry (e, struct child_parent_status, elem);

		lock_acquire(&child->ref_count_lock);
		child->ref_count--;
		lock_release(&child->ref_count_lock);

		if (child->ref_count == 0)
			finished_progs[size++] = child;
	}

	for (int i = 0; i < size; i++)
		free (finished_progs[i]);
}

static void free_file_descriptors(struct process * prog) {
	for (int i = 3; i < MAX_OPENED_FILES; ++i) {
		if (prog->file_table[i] != NULL) {
			file_close(prog->file_table[i]);
			prog->file_table[i] = NULL;
		}
	}
}

static void free_acquired_locks(struct process * prog) {
	struct list* lst = &prog->locks;
	struct list_elem *e;
	while (!list_empty(lst)) {
		struct lock_info *l = list_entry (list_pop_front(lst), struct lock_info, elem);
		free(l);
	}

	lst = &prog->semaphores;
	while (!list_empty(lst)) {
		struct sema_info *l = list_entry (list_pop_front(lst), struct sema_info, elem);
		free(l);
	}
}

/* Free the current process's resources. */
void process_exit(void) {
	struct process * prog = thread_current()->pcb;
	struct thread * cur = thread_current();
	// handle special case when main thread being terminated, then need to follow the pthreads rule
	if (is_main_thread(cur, cur->pcb)) {
		pthread_exit_main();
	}

	printf("%s: exit(%d)\n", prog->process_name, prog->cps->exit_code);

	/* If this thread does not have a PCB, don't worry */
	if (prog == NULL) {
		thread_exit();
		NOT_REACHED();
	}

	/* adjust the ref_count between the parent and child processes */
	lock_acquire (&(prog->cps->ref_count_lock));
	(prog->cps->ref_count)--;
	lock_release (&(prog->cps->ref_count_lock));
	if (prog->cps->ref_count == 0)
		free (prog->cps);

	/* Signal the parent in process_wait() */
	sema_up (&(prog->cps->wait_sema));

	/* Destroy the current process's page directory and switch back
	   to the kernel-only page directory. */
	uint32_t *pd = cur->pcb->pagedir;
	if (pd != NULL) {
		/* Correct ordering here is crucial.  We must set
			 cur->pcb->pagedir to NULL before switching page directories,
			 so that a timer interrupt can't switch back to the
			 process page directory.  We must activate the base page
			 directory before destroying the process's page
			 directory, or our active page directory will be one
			 that's been freed (and cleared). */
		cur->pcb->pagedir = NULL;
		pagedir_activate(NULL);
		pagedir_destroy(pd);
	}

	/* Remove all child from children list, free up memory. */
	free_finished_processes (&prog->child_process_list);
	/* Close all opened files   */
	free_file_descriptors (prog);

	// free acquired locks and semaphores
	free_acquired_locks(prog);
	// free it's executable
	file_close(cur->pcb->executable_file);

	/* Free the PCB of this process and kill this thread
	   Avoid race where PCB is freed before t->pcb is set to NULL
	   If this happens, then an unfortuantely timed timer interrupt
	   can try to activate the pagedir, but it is now freed memory */
	struct process *pcb_to_free = cur->pcb;
	cur->pcb = NULL;
	free(pcb_to_free);

	thread_exit();
}

/* Sets up the CPU for running user code in the current
   thread. This function is called on every context switch. */
void process_activate(void) {
	struct thread *t = thread_current();

	/* Activate thread's page tables. */
	if (t->pcb != NULL && t->pcb->pagedir != NULL)
		pagedir_activate(t->pcb->pagedir);
	else
		pagedir_activate(NULL);

	/* Set thread's kernel stack for use in processing interrupts.
	   This does nothing if this is not a user process. */
	tss_update();
}

/* We load ELF binaries.  The following definitions are taken
   from the ELF specification, [ELF1], more-or-less verbatim.  */

/* ELF types.  See [ELF1] 1-2. */
typedef uint32_t Elf32_Word, Elf32_Addr, Elf32_Off;
typedef uint16_t Elf32_Half;

/* For use with ELF types in printf(). */
#define PE32Wx PRIx32 /* Print Elf32_Word in hexadecimal. */
#define PE32Ax PRIx32 /* Print Elf32_Addr in hexadecimal. */
#define PE32Ox PRIx32 /* Print Elf32_Off in hexadecimal. */
#define PE32Hx PRIx16 /* Print Elf32_Half in hexadecimal. */

/* Executable header.  See [ELF1] 1-4 to 1-8.
   This appears at the very beginning of an ELF binary. */
struct Elf32_Ehdr {
  unsigned char e_ident[16];
  Elf32_Half e_type;
  Elf32_Half e_machine;
  Elf32_Word e_version;
  Elf32_Addr e_entry;
  Elf32_Off e_phoff;
  Elf32_Off e_shoff;
  Elf32_Word e_flags;
  Elf32_Half e_ehsize;
  Elf32_Half e_phentsize;
  Elf32_Half e_phnum;
  Elf32_Half e_shentsize;
  Elf32_Half e_shnum;
  Elf32_Half e_shstrndx;
};

/* Program header.  See [ELF1] 2-2 to 2-4.
   There are e_phnum of these, starting at file offset e_phoff
   (see [ELF1] 1-6). */
struct Elf32_Phdr {
  Elf32_Word p_type;
  Elf32_Off p_offset;
  Elf32_Addr p_vaddr;
  Elf32_Addr p_paddr;
  Elf32_Word p_filesz;
  Elf32_Word p_memsz;
  Elf32_Word p_flags;
  Elf32_Word p_align;
};

/* Values for p_type.  See [ELF1] 2-3. */
#define PT_NULL 0           /* Ignore. */
#define PT_LOAD 1           /* Loadable segment. */
#define PT_DYNAMIC 2        /* Dynamic linking info. */
#define PT_INTERP 3         /* Name of dynamic loader. */
#define PT_NOTE 4           /* Auxiliary info. */
#define PT_SHLIB 5          /* Reserved. */
#define PT_PHDR 6           /* Program header table. */
#define PT_STACK 0x6474e551 /* Stack segment. */

/* Flags for p_flags.  See [ELF3] 2-3 and 2-4. */
#define PF_X 1 /* Executable. */
#define PF_W 2 /* Writable. */
#define PF_R 4 /* Readable. */

static bool setup_stack(void **esp);
static bool validate_segment(const struct Elf32_Phdr *, struct file *);
static bool load_segment(struct file *file, off_t ofs, uint8_t *upage, uint32_t read_bytes,
                         uint32_t zero_bytes, bool writable);

/* Loads an ELF executable from FILE_NAME into the current thread.
   Stores the executable's entry point into *EIP
   and its initial stack pointer into *ESP.
   Returns true if successful, false otherwise. */
bool load(const char *file_name, void (**eip)(void), void **esp) {
	struct thread *t = thread_current();
	struct Elf32_Ehdr ehdr;
	struct file *file = NULL;
	off_t file_ofs;
	bool success = false;
	int i;

	/* Allocate and activate page directory. */
	t->pcb->pagedir = pagedir_create();
	if (t->pcb->pagedir == NULL)
		goto done;
	process_activate();

//	lock_acquire(&file_sys_lock);
	/* Open executable file. */
	file = filesys_open(file_name);
//	lock_release(&file_sys_lock);

	if (file == NULL) {
		printf("load: %s: open failed\n", file_name);
		goto done;
	}

	// attach the executable file to the process
	t->pcb->executable_file = file;
	file_deny_write(file);



	/* Read and verify executable header. */
	if (file_read(file, &ehdr, sizeof ehdr) != sizeof ehdr ||
		memcmp(ehdr.e_ident, "\177ELF\1\1\1", 7) || ehdr.e_type != 2 || ehdr.e_machine != 3 ||
		ehdr.e_version != 1 || ehdr.e_phentsize != sizeof(struct Elf32_Phdr)
		|| ehdr.e_phnum > 1024) {
		printf("load: %s: error loading executable\n", file_name);
		goto done;
	}

	/* Read program headers. */
	file_ofs = ehdr.e_phoff;
	for (i = 0; i < ehdr.e_phnum; i++) {
		struct Elf32_Phdr phdr;

		if (file_ofs < 0 || file_ofs > file_length(file))
			goto done;
		file_seek(file, file_ofs);

		if (file_read(file, &phdr, sizeof phdr) != sizeof phdr)
			goto done;
		file_ofs += sizeof phdr;
		switch (phdr.p_type) {
			case PT_NULL:
			case PT_NOTE:
			case PT_PHDR:
			case PT_STACK:
			default:
				/* Ignore this segment. */
				break;
			case PT_DYNAMIC:
			case PT_INTERP:
			case PT_SHLIB:goto done;
			case PT_LOAD:
				if (validate_segment(&phdr, file)) {
					bool writable = (phdr.p_flags & PF_W) != 0;
					uint32_t file_page = phdr.p_offset & ~PGMASK;
					uint32_t mem_page = phdr.p_vaddr & ~PGMASK;
					uint32_t page_offset = phdr.p_vaddr & PGMASK;
					uint32_t read_bytes, zero_bytes;
					if (phdr.p_filesz > 0) {
						/* Normal segment.
								 Read initial part from disk and zero the rest. */
						read_bytes = page_offset + phdr.p_filesz;
						zero_bytes = (ROUND_UP(page_offset + phdr.p_memsz, PGSIZE) - read_bytes);
					} else {
						/* Entirely zero.
								 Don't read anything from disk. */
						read_bytes = 0;
						zero_bytes = ROUND_UP(page_offset + phdr.p_memsz, PGSIZE);
					}
					if (!load_segment(file,
					                  file_page,
					                  (void *) mem_page,
					                  read_bytes,
					                  zero_bytes,
					                  writable))
						goto done;
				} else
					goto done;
				break;
		}
	}

	/* Set up stack. */
	if (!setup_stack(esp))
		goto done;

	/* Start address. */
	*eip = (void (*)(void)) ehdr.e_entry;
	success = true;

	done:
	/* We arrive here whether the load is successful or not. */
//	file_close(file);
	return success;
}

/* load() helpers. */

static bool install_page(void *upage, void *kpage, bool writable);

/* Checks whether PHDR describes a valid, loadable segment in
   FILE and returns true if so, false otherwise. */
static bool validate_segment(const struct Elf32_Phdr *phdr, struct file *file) {
	/* p_offset and p_vaddr must have the same page offset. */
	if ((phdr->p_offset & PGMASK) != (phdr->p_vaddr & PGMASK))
		return false;

	/* p_offset must point within FILE. */
	if (phdr->p_offset > (Elf32_Off) file_length(file))
		return false;

	/* p_memsz must be at least as big as p_filesz. */
	if (phdr->p_memsz < phdr->p_filesz)
		return false;

	/* The segment must not be empty. */
	if (phdr->p_memsz == 0)
		return false;

	/* The virtual memory region must both start and end within the
	   user address space range. */
	if (!is_user_vaddr((void *) phdr->p_vaddr))
		return false;
	if (!is_user_vaddr((void *) (phdr->p_vaddr + phdr->p_memsz)))
		return false;

	/* The region cannot "wrap around" across the kernel virtual
	   address space. */
	if (phdr->p_vaddr + phdr->p_memsz < phdr->p_vaddr)
		return false;

	/* Disallow mapping page 0.
	   Not only is it a bad idea to map page 0, but if we allowed
	   it then user code that passed a null pointer to system calls
	   could quite likely panic the kernel by way of null pointer
	   assertions in memcpy(), etc. */
	if (phdr->p_vaddr < PGSIZE)
		return false;

	/* It's okay. */
	return true;
}

/* Loads a segment starting at offset OFS in FILE at address
   UPAGE.  In total, READ_BYTES + ZERO_BYTES bytes of virtual
   memory are initialized, as follows:

        - READ_BYTES bytes at UPAGE must be read from FILE
          starting at offset OFS.

        - ZERO_BYTES bytes at UPAGE + READ_BYTES must be zeroed.

   The pages initialized by this function must be writable by the
   user process if WRITABLE is true, read-only otherwise.

   Return true if successful, false if a memory allocation error
   or disk read error occurs. */
static bool load_segment(struct file *file, off_t ofs, uint8_t *upage, uint32_t read_bytes,
                         uint32_t zero_bytes, bool writable) {
	ASSERT((read_bytes + zero_bytes) % PGSIZE == 0);
	ASSERT(pg_ofs(upage) == 0);
	ASSERT(ofs % PGSIZE == 0);

	file_seek(file, ofs);
	while (read_bytes > 0 || zero_bytes > 0) {
		/* Calculate how to fill this page.
			 We will read PAGE_READ_BYTES bytes from FILE
			 and zero the final PAGE_ZERO_BYTES bytes. */
		size_t page_read_bytes = read_bytes < PGSIZE ? read_bytes : PGSIZE;
		size_t page_zero_bytes = PGSIZE - page_read_bytes;

		/* Get a page of memory. */
		uint8_t *kpage = palloc_get_page(PAL_USER);
		if (kpage == NULL)
			return false;

		/* Load this page. */
		if (file_read(file, kpage, page_read_bytes) != (int) page_read_bytes) {
			palloc_free_page(kpage);
			return false;
		}
		memset(kpage + page_read_bytes, 0, page_zero_bytes);

		/* Add the page to the process's address space. */
		if (!install_page(upage, kpage, writable)) {
			palloc_free_page(kpage);
			return false;
		}

		/* Advance. */
		read_bytes -= page_read_bytes;
		zero_bytes -= page_zero_bytes;
		upage += PGSIZE;
	}
	return true;
}

/* Create a minimal stack by mapping a zeroed page at the top of
   user virtual memory. */
static bool setup_stack(void **esp) {
	uint8_t *kpage;
	bool success = false;

	kpage = palloc_get_page(PAL_USER | PAL_ZERO);
	if (kpage != NULL) {
		success = install_page(((uint8_t *) PHYS_BASE) - PGSIZE, kpage, true);
		if (success)
			*esp = PHYS_BASE;
		else
			palloc_free_page(kpage);
	}
	return success;
}

/* Adds a mapping from user virtual address UPAGE to kernel
   virtual address KPAGE to the page table.
   If WRITABLE is true, the user process may modify the page;
   otherwise, it is read-only.
   UPAGE must not already be mapped.
   KPAGE should probably be a page obtained from the user pool
   with palloc_get_page().
   Returns true on success, false if UPAGE is already mapped or
   if memory allocation fails. */
static bool install_page(void *upage, void *kpage, bool writable) {
	struct thread *t = thread_current();

	/* Verify that there's not already a page at that virtual
	   address, then map our page there. */
	return (pagedir_get_page(t->pcb->pagedir, upage) == NULL &&
		pagedir_set_page(t->pcb->pagedir, upage, kpage, writable));
}

/* Returns true if t is the main thread of the process p */
bool is_main_thread(struct thread *t, struct process *p) { return p->main_thread == t; }

/* Gets the PID of a process */
pid_t get_pid(struct process *p) { return (pid_t) p->main_thread->tid; }

/* Creates a new stack for the thread and sets up its arguments.
   Stores the thread's entry point into *EIP and its initial stack
   pointer into *ESP. Handles all cleanup if unsuccessful. Returns
   true if successful, false otherwise.

   This function will be implemented in Project 2: Multithreading. For
   now, it does nothing. You may find it necessary to change the
   function signature. */
bool setup_thread(void (**eip)(void) UNUSED, void **esp UNUSED,
									struct start_pthread_args * args) {
	struct thread *t = thread_current ();
	// set the ip points to the stub_fun sf. Which is a wrapper that call pthead_exit()
	// after the function completed
	*eip = args->sf;

	uint8_t* kpage;
	bool success = false;
	// allocate a page from physical memory pool
	kpage = palloc_get_page(PAL_USER | PAL_ZERO);
	// try to map the page
	if (kpage != NULL) {
		size_t num_pthreads = t->pcb->num_pthreads;
		// the default stack has already takes 1 page
		void * upage = ((uint8_t*)PHYS_BASE) - PGSIZE * (num_pthreads + 2);
		success = install_page(upage, kpage, true);
		if (success)
			*esp = PHYS_BASE - PGSIZE *(num_pthreads + 1);
		else
			palloc_free_page(kpage);
	}
	// set up stack for arguments
	if (success) {
		uintptr_t* esp_prime = (uintptr_t*)*esp - 2; // 16 bytes aligned
		// set up arguments for the function args->sf to runs
			// void _pthread_start_stub(pthread_fun fun, void* arg)
		*(--esp_prime) = args->arg;
		*(--esp_prime) = args->tf;
		*(--esp_prime) = 0; // fake pointer
		// set the esp
		*esp = esp_prime;
	}

	return success;
}

/* Starts a new thread with a new user stack running SF, which takes
   TF and ARG as arguments on its user stack. This new thread may be
   scheduled (and may even exit) before pthread_execute () returns.
   Returns the new thread's TID or TID_ERROR if the thread cannot
   be created properly.

   This function will be implemented in Project 2: Multithreading and
   should be similar to process_execute (). For now, it does nothing.
   */
tid_t pthread_execute(stub_fun sf UNUSED, pthread_fun tf UNUSED, void *arg UNUSED) {
	tid_t tid;

	struct start_pthread_args args;
	struct thread *t = thread_current ();
	// init
	args.sf = sf;
	args.tf = tf;
	args.arg = arg;
	args.success = true;
	sema_init(&(args.sema), 0);
	// make the name for the thread
	char *prefix = "pthread-";
	char * tname = malloc(strlen(t->name) + strlen(prefix) + 1);
	strlcpy(tname, prefix, strlen(prefix) + 1);
	strlcat(tname, t->name, strlen(t->name) + strlen(prefix) + 1);
	/* Create a new thread to execute FILE_NAME. */
	tid = thread_create(tname, PRI_DEFAULT, start_pthread, &args);

	if (tid != TID_ERROR) {
		sema_down(&args.sema);
		if(!args.success)
			return TID_ERROR;
	}

	return tid;
}

/* A thread function that creates a new user thread and starts it
   running. Responsible for adding itself to the list of threads in
   the PCB.

   This function will be implemented in Project 2: Multithreading and
   should be similar to start_process (). For now, it does nothing. */
static void start_pthread(void *exec_ UNUSED) {
	struct thread *t = thread_current();
	struct start_pthread_args * args = (struct start_pthread_args *) exec_;

	/* Initialize interrupt frame and load executable. */
	struct intr_frame if_;
	memset(&if_, 0, sizeof if_);
	if_.gs = if_.fs = if_.es = if_.ds = if_.ss = SEL_UDSEG;
	if_.cs = SEL_UCSEG;
	if_.eflags = FLAG_IF | FLAG_MBS;
	// call setup_thread
	args->success = setup_thread(&if_.eip, &if_.esp, args);

	/* Added FPU code */
	int FPU_SIZE = 108;
	uint8_t fpu1[FPU_SIZE];
	uint8_t init_fpu1[FPU_SIZE];
	asm("fsave (%0); fninit; fsave (%1); frstor (%0)" : : "g"(&fpu1), "g"(&init_fpu1));
	for (int i = 0; i < FPU_SIZE; i++) {
		if_.st[i] = init_fpu1[i];
	}
	// signal the parent
	sema_up (&args->sema);
	if (!args->success)
		thread_exit();
	/*
	 * Add the pthread info into the process
	 */
	struct pthread_info * p = malloc(sizeof(struct pthread_info));
	p->pid = t->tid;
	p->is_exit = false;
	p->is_joined = false;
	sema_init(&p->sema, 0);
	lock_init(&p->lock);
	list_push_back (&(t->pcb->pthreads), &(p->elem));
	// increase the number of pthreads count in the process
	t->pcb->num_pthreads++;
	// add the info into the thread
	t->join_info = p;

	/* Start the user process by simulating a return from an
	 interrupt, implemented by intr_exit (in
	 threads/intr-stubs.S).  Because intr_exit takes all of its
	 arguments on the stack in the form of a `struct intr_frame',
	 we just point the stack pointer (%esp) to our stack frame
	 and jump to it. */
	asm volatile("movl %0, %%esp; jmp intr_exit" : : "g"(&if_) : "memory");
	NOT_REACHED();
}

/* Waits for thread with TID to die, if that thread was spawned
   in the same process and has not been waited on yet. Returns TID on
   success and returns TID_ERROR on failure immediately, without
   waiting.

   This function will be implemented in Project 2: Multithreading. For
   now, it does nothing. */
tid_t pthread_join(tid_t tid UNUSED) {
	struct thread *t = thread_current();
	struct thread *main_thread = t->pcb->main_thread;
	if (tid == TID_ERROR || tid == t->tid) {
		return TID_ERROR;
	}


	// want to join on main-thread
	if (tid == main_thread->tid) {
//		if (main_thread->join_info->is_joined)
//			return TID_ERROR;
//
//		main_thread->join_info->is_joined = true;
		// put the thread to sleep
		sema_down(&t->pcb->main_thread_join_sema);
		return tid;
	}

	bool found = false;
	struct pthread_info * child;
	for (struct list_elem *e = list_begin (&(t->pcb->pthreads)); e != list_end (&(t->pcb->pthreads)); e = list_next (e))
	{
		child = list_entry (e, struct pthread_info, elem);
		if (child->pid == (pid_t) tid && !child->is_joined) {
			found = true;
			break;
		}
	}

	if (!found)
		return TID_ERROR;

	if (!child->is_exit) {
		// join
		child->is_joined = true;
		sema_down(&child->sema);
		// then when this thread runs, it means the child has finished
		//		child->is_exit = true;
	}

	return child->pid;
}

/* Free the current thread's resources. Most resources will
   be freed on thread_exit(), so all we have to do is deallocate the
   thread's userspace stack. Wake any waiters on this thread.

   The main thread should not use this function. See
   pthread_exit_main() below.

   This function will be implemented in Project 2: Multithreading. For
   now, it does nothing. */
void pthread_exit(const void* user_stack) {
	struct thread *t = thread_current();
	// deallocate the thread's userspace stack
	void * page = pg_round_down(user_stack); // down or up?
	// free the page from the kernel pool
	palloc_free_page(pagedir_get_page(t->pcb->pagedir, page));
	// remove from the mapping
	pagedir_clear_page(t->pcb->pagedir, page);
	// wake up waiter
	sema_up(&t->join_info->sema);
	t->join_info->is_exit = true;
	// call exit
	thread_exit();
}

/* Only to be used when the main thread explicitly calls pthread_exit.
   The main thread should wait on all threads in the process to
   terminate properly, before exiting itself. When it exits itself, it
   must terminate the process in addition to all necessary duties in
   pthread_exit.

   This function will be implemented in Project 2: Multithreading. For
   now, it does nothing. */
void pthread_exit_main(void) {
	struct thread *t = thread_current();
	// signal the waiter
	sema_up(&t->pcb->main_thread_join_sema);
	struct list* lst = &t->pcb->pthreads;
	struct list_elem *e;
	while (!list_empty(lst)) {
		struct pthread_info * child = list_entry (list_pop_front(lst), struct pthread_info, elem);
		if (!child->is_joined) {
			sema_down(&child->sema);
		}
		free(child);
	}
}