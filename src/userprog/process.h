#ifndef USERPROG_PROCESS_H
#define USERPROG_PROCESS_H

#include "threads/thread.h"
#include "filesys/file.h"
#include <stdint.h>

// At most 8MB can be allocated to the stack
// These defines will be used in Project 2: Multithreading
#define MAX_STACK_PAGES (1 << 11)
#define MAX_THREADS 127
#define MAX_OPENED_FILES 128
/* PIDs and TIDs are the same type. PID should be
   the TID of the main thread of the process */
typedef tid_t pid_t;

/* Thread functions (Project 2: Multithreading) */
typedef void (*pthread_fun)(void *);
typedef void (*stub_fun)(pthread_fun, void *);

/* The process control block for a given process. Since
   there can be multiple threads per process, we need a separate
   PCB from the TCB. All TCBs in a process will have a pointer
   to the PCB, and the PCB will have a pointer to the main thread
   of the process, which is `special`. */
struct process {
  /* Owned by process.c. */
  uint32_t *pagedir;          /* Page directory. */
  char process_name[16];      /* Name of the main thread */
  struct thread *main_thread; /* Pointer to main thread */
  struct file *file_table[MAX_OPENED_FILES];
  struct file * executable_file;
  struct list child_process_list;
	// every process has this, to communicate between it and its parent
  struct child_parent_status* cps;
	// the list of child pthreads of the process
	struct list pthreads;
	// how many pthreads this process has
	int num_pthreads;
	// the sema for thread that is join on the main thread
	struct semaphore main_thread_join_sema;
	int exit_status; //the exit status of the process
	struct list locks; // list of acquired locks
	struct list semaphores; // list of acquired semaphores
};

struct child_parent_status {
  pid_t pid;
  int exit_code;
  struct list_elem elem;
  struct semaphore wait_sema;              /* Init with 0. */
  int ref_count;                      /* Init with 2, showing number of threads working with this status. */
  struct lock ref_count_lock;                   /* For locking ref_count. */
};

struct pthread_info {
	pid_t pid;
	bool is_exit;
	bool is_joined; // is it being joined by some other thread already
	struct list_elem elem;
	// init to 0, use for the call thread_join on this thread for the calling to sleep
	struct semaphore sema;
	struct lock lock;
};

struct process_args {
  char *fn;                           /* File name. */
  struct child_parent_status *cps;    /* Child parent status. */
  bool success;                       /* Boolean to show load result. */
};

struct start_pthread_args {
	bool success;                       /* Boolean to show load result. */
	stub_fun sf;
	pthread_fun tf;
	void *arg;
	struct semaphore sema;
};

void userprog_init(void);

pid_t process_execute(const char *file_name);
int process_wait(pid_t);
void process_exit(void);
void process_activate(void);
bool is_main_thread(struct thread *, struct process *);
pid_t get_pid(struct process *);

tid_t pthread_execute(stub_fun, pthread_fun, void *);
tid_t pthread_join(tid_t);
void pthread_exit(const void* user_stack);
void pthread_exit_main(void);
#endif /* userprog/process.h */
